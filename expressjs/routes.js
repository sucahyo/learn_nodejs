'use strict';

module.exports = function (app) {
    var controller  = require('./controllers/controller');
    var user        = require('./controllers/user');

    app.route('/')
        .get(controller.index);

    app.route('/users')
        .get(user.list);

    app.route('/user/:id')
        .get(user.findUsers);

    app.route('/user')
        .post(user.createUsers);

    app.route('/user/:id')
        .put(user.updateUsers);

    app.route('/user/:id')
        .delete(user.deleteUsers);

};
