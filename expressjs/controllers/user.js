'use strict';

var response = require('../response');
var db = require('../conn');

exports.list = function (req, res) {
    db.query('SELECT * FROM students', function (error, rows, fields) {
        if (error) {
            console.log(error)
        } else {
            response.ok(rows, res)
        }
    });
};

exports.findUsers = function (req, res) {
    db.query('SELECT * FROM students where ? ',
        { id: req.params.id },
        function (error, rows, fields) {
            if (error) {
                console.log(error)
            } else {
                response.ok(rows, res)
            }
        });
};

exports.createUsers = function (req, res) {

    console.log('req', req);
    db.query('INSERT INTO students SET ? ', {
        name: req.body.name,
        address: req.body.address,
    },
        function (error, rows, fields) {
            if (error) {
                console.log(error)
            } else {
                response.ok("Berhasil menambahkan user!", res)
            }
        });
};

exports.updateUsers = function (req, res) {
    db.query('UPDATE students SET ? WHERE ? ', [
        {
            name: req.body.name,
            address: req.body.address,
        },
        {
            id: req.params.id,
        }],
        function (error, rows, fields) {
            if (error) {
                console.log(error)
            } else {
                response.ok("Berhasil merubah user!", res)
            }
        });
};

exports.deleteUsers = function (req, res) {
    db.query('DELETE FROM students WHERE ? ',
        { id: req.params.id },
        function (error, rows, fields) {
            if (error) {
                console.log(error)
            } else {
                response.ok("Berhasil menghapus user!", res)
            }
        });
};
