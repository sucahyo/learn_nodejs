var http = require("http");
var fs = require("fs");

http.createServer(function (req, res) {
    var file, code;
    switch (req.url) {
        case "/":
            code = 200;
            file = "./template/index.html";
            break;
        case "/contact":
            code = 200;
            file = "./template/contact.html";
            break;
        default:
            code = 400;
            file = "./template/errors/404.html";
            break;
    }

    res.writeHead(code, { "Content-Type": "text/html" });
    fs.createReadStream(file).pipe(res);
}).listen('8888');

console.log('Server is running...');