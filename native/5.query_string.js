var http = require("http");
var fs = require("fs");
var url = require("url");
var qString = require("querystring");

http.createServer(function (req, res) {
    if (req.url != "/favicon.ico") {
        var access = url.parse(req.url)
        var data = qString.parse(access.query)

        if (access.pathname === "/") {
            res.writeHead(200, { "Content-Type": "text/plain" });
            res.end(JSON.stringify(data));
        } else if (access.pathname === "/form") {
            if (req.method.toUpperCase() === 'POST') {
                var dataPost = "";
                req.on('data', function (chunk) {
                    dataPost += chunk;
                });
                req.on('end', function () {
                    dataPost= qString.parse(dataPost);
                    res.writeHead(200, { "Content-Type": "text/plain" });
                    res.end(JSON.stringify(dataPost));
                });
            } else if (req.method.toUpperCase() === 'GET') {
                res.writeHead(200, { "Content-Type": "text/html" });
                fs.createReadStream("./template/form.html").pipe(res);
            }
        } else {
            res.writeHead(400, { "Content-Type": "text/plain" });
            res.write("Error 404!");
            res.end();
        }
    }
}).listen('8888');

console.log('Server is running...');