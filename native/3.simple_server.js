var http = require("http");

http.createServer(function(req, res) {
    if(req.url != "/favicon.ico"){
        res.writeHead(200, {"Content-Type" : "text/plain"});
        res.write("Hello from Node Js Server\n");
        res.write("Your req is: "+ req.url );
        res.end();
    }
}).listen('8888');

console.log('Server is running...');