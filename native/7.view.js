var http = require("http");
var url = require("url");
var routes = require("routes")();
var view = require("swig");

routes.addRoute('/', function(req, res){
    var html = view.compileFile('./template/index.html')({
        title: "Index page sucahyo"
    });
    res.writeHead(200, {"Content-Type" : "text/html"});
    res.end(html);
});

routes.addRoute('/contact', function(req, res){
    var html = view.compileFile('./template/contact.html')();
    res.writeHead(200, {"Content-Type" : "text/html"});
    res.end(html);
});


http.createServer(function (req, res) {
    var path = url.parse(req.url).pathname;
    var match = routes.match(path);

    if(match){
        match.fn(req, res);
    }else{
        res.writeHead(200, {"Content-Type" : "text/plain"});
        res.write("Page Note Found!");
        res.end();

    }
}).listen('8888');

console.log('Server is running...');