var mysql = require("mysql");

module.exports = {
    'db': mysql.createConnection({
        host: 'localhost',
        port: 3306,
        database: 'learn_nodejs',
        user: 'root',
        password: '',
    })
}