var http = require("http");
var url = require("url");
var routes = require("routes")();

routes.addRoute('/', function(req, res){
    res.writeHead(200, {"Content-Type" : "text/plain"});
    res.end('Index Page');
});

routes.addRoute('/profile/:name?/:city?', function(req, res){
    res.writeHead(200, {"Content-Type" : "text/plain"});

    var value = "Profile Page \n";
    if(this.params.name){
        value += "Name: "+this.params.name;
        value += "\n";
    }
    if(this.params.city){
        value += "City: "+this.params.city;
        value += "\n";
    }
    res.end(value);
});

http.createServer(function (req, res) {
    var path = url.parse(req.url).pathname;
    var match = routes.match(path);

    if(match){
        match.fn(req, res);
    }else{
        res.writeHead(200, {"Content-Type" : "text/plain"});
        res.write("Page Note Found!");
        res.end();

    }
}).listen('8888');

console.log('Server is running...');