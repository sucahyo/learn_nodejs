var env = require('./ENV');

var http = require("http");
var url = require("url");
var routes = require("routes")();
var view = require("swig");
var db = env.db;

routes.addRoute('/', function (req, res) {
    db.query("select * from students", function (err, rows, field) {
        if (err) throw err;

        var list = "";
        rows.forEach((v, i) => {
            list += "<li>ID: " + v.id + ", Name: " + v.name + ", Address: " + v.address + "</li>";
        });

        res.writeHead(200, { "Content-Type": "text/html" });
        res.end("<h2>Read Data</h2><ol>" + list + "</ol>");
    });
});

routes.addRoute('/insert', function (req, res) {
    db.query("INSERT INTO students set  ? ", {
        name: "Kepin",
        address: "Kedanyang "
    }, function (err, field) {
        if (err) throw err;

        res.writeHead(200, { "Content-Type": "text/html" });
        res.end("<h2>Insert Data</h2>" + field.affectedRows + " Affected Rows");
    });
});

routes.addRoute('/update', function (req, res) {
    db.query("UPDATE students SET ? WHERE ? ", [
        {name : 'Nafi 3'},
        {id : 7}
    ], function (err, field) {
        if (err) throw err;

        res.writeHead(200, { "Content-Type": "text/html" });
        res.end("<h2>Update Data</h2>" + field.changedRows + " Changed Rows");
    });
});

routes.addRoute('/delete/:id', function (req, res) {
    db.query("DELETE FROM students WHERE ? ",
        {id : this.params.id}
        , function (err, field) {
        if (err) throw err;

        res.writeHead(200, { "Content-Type": "text/html" });
        res.end("<h2>Delete Data</h2>" + field.affectedRows + " Deleted Rows");
    });
});


http.createServer(function (req, res) {
    var path = url.parse(req.url).pathname;
    var match = routes.match(path);

    if (match) {
        match.fn(req, res);
    } else {
        var html = view.compileFile('./template/errors/404.html')({
            title: "Error 404"
        });
        res.writeHead(200, { "Content-Type": "text/html" });
        res.end(html);
    }
}).listen('8888');

console.log('Server is running...');